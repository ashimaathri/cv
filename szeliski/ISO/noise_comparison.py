#! /usr/bin/env python
import os
import math

import rawpy
import numpy as np
from skimage import io, color, filters, morphology, viewer
from skimage.viewer import ImageViewer


def image_filenames(path=".", extension=".jpg"):
    for root, dirs, files in os.walk(path):
        for filename in files:
            if filename.endswith(extension):
                yield os.path.join(path, filename)

def variation():
    iso_levels = ["200", "400", "800", "1600"]
    iso_variance = {'r': {}, 'g': {}, 'b': {}}
    for level in iso_levels:
        path = "/home/ashima/Downloads/ComputerVision/ISO_noise/" + level
        images = [rawpy.imread(f).postprocess() for f in image_filenames(path, ".dng")]
        print(images[0].shape)
        mean = np.zeros(images[0].shape)
        noise = np.zeros(images[0].shape)
        for n, image in enumerate(images, start = 1):
            mean += (image - mean) / n
        for image in images:
            noise += image - mean
        # We want to find the variance of the errors/noise.
        mean_noise = {
                'r': np.mean(noise[:, :, 0]),
                'g': np.mean(noise[:, :, 1]),
                'b': np.mean(noise[:, :, 2]) }
        print(f"Mean noise for iso level %s:" % level, mean_noise) # Should be close to 0
        noise_squared = np.power(noise, 2)
        size = noise.shape[0] * noise.shape[1];
        print(size)
        iso_variance['r'][level] = np.sum(noise_squared[:, :, 0]) / size 
        iso_variance['g'][level] = np.sum(noise_squared[:, :, 1]) / size
        iso_variance['b'][level] = np.sum(noise_squared[:, :, 2]) / size
        print(iso_variance['r'][level])
        print(iso_variance['g'][level])
        print(iso_variance['b'][level])
        #ImageViewer(mean.astype(int)).show()

    # Get ISO level of image from metadata
    # Create dict with keys as iso levels and values as arrays of filenames with that iso level
    # Create dict for with keys as iso levels and value as variance

variation()
