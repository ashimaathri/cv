import numpy as np

def identity(size):
    assert size % 2 == 1, "size should be odd"

    mask = np.zeros((size, size))
    middle = size // 2
    mask[middle, middle] = 1

    return mask

def gaussian(size, sigma):
    assert size % 2 == 1, "size should be odd"

    mask = np.zeros((size, size), np.float32)
    middle = size // 2
    
    # x and y are offsets from the middle of the kernel
    for x in range(-middle, middle + 1):
        for y in range(-middle, middle + 1):
            sigma_sq = sigma ** 2
            numerator = np.exp(-(x ** 2 + y ** 2) / (2 * sigma_sq))
            denominator = 2 * np.pi * sigma_sq
            mask[middle + x, middle + y] = numerator / denominator

    return mask
