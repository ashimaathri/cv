#! /usr/bin/env python
import numpy as np
from skimage import io, viewer

import masks
import operators

def gaussian(image, mask):
    return np.dstack([operators.convolve2D(image[:, :, channel], mask)
        for channel in range(image.shape[2])])

output = gaussian(io.imread('./test3.jpeg'), masks.gaussian(3, 1))
viewer.ImageViewer(output.astype(int)).show()
