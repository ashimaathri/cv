import numpy as np
from scipy import linalg
from skimage.util import shape

import masks

# XXX Obsolete. This is too inefficient to be used, it constructs huge matrices
# and uses too much RAM
def convolve2D_toeplitz(matrix, kernel):
    output_rows = matrix.shape[0] + kernel.shape[0] - 1
    output_cols = matrix.shape[1] + kernel.shape[1] - 1

    print("Padding the kernel", output_rows, output_cols)
    # Pad filter
    padded_kernel = np.pad(kernel,
            ((output_rows - kernel.shape[0], 0),
             (0, output_cols - kernel.shape[1])),
            'constant',
            constant_values = 0)

    print("Constructing the doubly blocked kernel")
    # Construct block toeplitz
    toeplitz_list = []

    for row_index in range(output_rows - 1, -1, -1):
        row = padded_kernel[row_index, :]
        first_row_toeplitz = np.r_[row[0], np.zeros(matrix.shape[1] - 1)]
        toeplitz_list.append(linalg.toeplitz(row, first_row_toeplitz))

    block_indices = linalg.toeplitz(
            range(1, output_rows + 1),
            np.r_[1, np.zeros(matrix.shape[0] - 1)])

    block_height, block_width = toeplitz_list[0].shape
    doubly_blocked = np.zeros((
            block_height * block_indices.shape[0],
            block_width * block_indices.shape[1]))

    for i in range(block_indices.shape[0]):
        for j in range(block_indices.shape[1]):
            start_i = i * block_height
            start_j = j * block_width
            end_i = start_i + block_height
            end_j = start_j + block_width
            doubly_blocked[start_i : end_i, start_j : end_j] = toeplitz_list[int(block_indices[i, j]) - 1]

    print("Vectorizing")
    # Vectorize input
    vectorized_input = np.ndarray.flatten(np.flipud(matrix))

    print("Matrix product", doubly_blocked.shape, vectorized_input.shape)
    # Matrix product
    vectorized_output = np.matmul(doubly_blocked, vectorized_input)

    # Reshaping
    return np.flipud(np.reshape(vectorized_output, [output_rows, output_cols]))

# XXX Obsolete (replaced by view as window mechanism)
def vectorize(padded_image, kernel_shape, output_shape):
    windows = []
    midpoint = kernel_shape[0] // 2  # assuming square kernel here

    for r in range(midpoint, padded_image.shape[0] - 1):
        for c in range(midpoint, padded_image.shape[1] - 1):
            window = padded_image[r - midpoint : r + midpoint + 1, c - midpoint : c + midpoint + 1]
            windows.append(window.flatten())

    return np.transpose(np.array(windows), kernel_shape)

def convolve2D(image, kernel):
    flipped_kernel = np.fliplr(np.flipud(kernel))

    padding = kernel.shape[0] // 2  # assuming square kernel here
    padded_image = np.pad(image,
            ((padding, padding),
             (padding, padding)),
            'constant',
            constant_values = 0)

    vectorized_input = shape.view_as_windows(
            padded_image,
            kernel.shape).reshape(
                    image.shape[0] * image.shape[1],
                    kernel.shape[0] ** 2).transpose()

    return np.dot(flipped_kernel.flatten(), vectorized_input).reshape(image.shape)
