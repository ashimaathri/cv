#! /usr/bin/env python
import os

import numpy as np
import matplotlib.pyplot as plt
from skimage import io, color, filters, morphology

# Get all images (*.jpg) in the directory
def images(path="."):
    for root, dirs, files in os.walk(path):
        for filename in files:
            if filename.endswith(".jpg"):
                yield os.path.join(path, filename)

# Find the middle pixel of the images
def middle_pixel(img):
    [numRows, numCols, _] = img.shape
    return [numRows // 2, numCols // 2]

def compare_intensities():
    ev1 = 1 # 1 is the highest possible intensity
    center = None
    intensities = []

    for filename in images():
        img = io.imread(filename)

        if not center:
            center = middle_pixel(img)
            print("Middle pixel is at", center)

        intensity = color.rgb2gray(img)[center[0], center[1]]

        intensities.append((filename, intensity))

        if intensity < ev1:
            ev1 = intensity

    for filename, intensity in intensities:
        print(filename, intensity / ev1)

compare_intensities()

# For each image, 
#  1. Convert to grayscale
#  2. Find the intensity at midpoint (and print)
#  3. Calculate multiplier (after figuring out the reference image with EV1) and print

