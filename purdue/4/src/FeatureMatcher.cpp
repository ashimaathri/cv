#include "homography.hpp"
#include <opencv2/core.hpp>
using namespace cv;
using namespace std;

void getFeatures() {
}

void harrisDetector() {
}

void ncc() {
}

void ssd() {
}

void getMatchedFeaturePairs() {
}

int main(int argc, char** argv ) {
  if (argc != 2) {
    printf("usage: %s <Image_Path>\n", argv[0]);
    return -1;
  }

  Mat image = imread(argv[1], 1);
  if(!image.data) {
    printf("No image data \n");
    return -1;
  }

  // Use harris corner detector to find features in each of the given images
  // Use ssd to find matching pairs
  // Use ncc to find matching pairs
  // Draw lines to indicate correspondences

  return 0;
}
